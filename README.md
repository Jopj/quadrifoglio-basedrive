# Quadrifoglio

Basedrive package for the Quadrifoglio robot! Takes a ROS twist message and publishes the native Quadrifoglio base drive command.
